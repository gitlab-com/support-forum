# GitLab.com Support Issue Tracker

**NOTE:** This project has been archived and put into read only mode.

If the issue you're facing is a _reproducible_ bug with the GitLab product, please search for an existing issue on the [GitLab project tracker](https://gitlab.com/gitlab-org/gitlab/issues) or [file a bug issue](https://gitlab.com/gitlab-org/gitlab/issues/new?issuable_template=Bug).

For issues with related products, please search for an existing issue or file a new issue in the appropriate project tracker, such as the [GitLab runner tracker](https://gitlab.com/gitlab-org/gitlab-runner/issues/).

If you're in a **paid** tier on GitLab.com or GitLab Self-managed, you're entitled to Support. Please open [a support ticket](https://support.gitlab.com) after reviewing our [Statement of Support](https://about.gitlab.com/support/)

If you're a *free* user on GitLab.com and are having one of the following specific issues please file [a support ticket](https://support.gitlab.com) after reviewing the [Free Plan Users Section](https://about.gitlab.com/support/statement-of-support.html#free-plan-users) in our [Statement of Support](https://about.gitlab.com/support/)

- Locked out of account due to two factor authentication, or not receiving forget password email.
- Inactive namespace request.
- Other group or project specific broken state that requires admin access.

For general questions, use cases, or anything else that does not fit into one of the above cases, please post in the [GitLab Forum](https://forum.gitlab.com) or on a third-party help site.
